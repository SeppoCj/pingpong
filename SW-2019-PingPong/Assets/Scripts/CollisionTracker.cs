﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionTracker : MonoBehaviour
{
    public GameManager gmScript;

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.name == "Ball")
        {
            gmScript.CountCollision();
        }

    }
   
}
