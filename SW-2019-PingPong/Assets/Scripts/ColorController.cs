﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorController : MonoBehaviour
{
    Color startColor;
    public Color redTint;
    public Color blueTint;
    SpriteRenderer sr;
    //public bool isRed;
    public bool isBlue;
    GameManager gmScript;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        gmScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (isBlue && !gmScript.teamOneWins && !gmScript.teamTwoWins)
        {
            sr.color = blueTint;

        }
        else if (!isBlue && !gmScript.teamOneWins && !gmScript.teamTwoWins)
        {
            sr.color = redTint;
        }
        startColor = sr.color;
    }

    // Update is called once per frame
    void Update()
    {

        if (isBlue && !gmScript.teamOneWins && !gmScript.teamTwoWins)
        {
            sr.color = blueTint;
        }
        else if (!isBlue && !gmScript.teamOneWins && !gmScript.teamTwoWins)
        {
            sr.color = redTint;
        }
        else
        {
            sr.color = Color.black;
        }
    }

    public void SwitchColor()
    {
       
        if (isBlue)
        {
            isBlue = false;
            sr.color = redTint;
        }
        else
        {
            isBlue = true;
            sr.color = blueTint;
        }

    }
  
}

