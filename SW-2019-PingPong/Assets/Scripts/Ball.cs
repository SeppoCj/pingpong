﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    // Start is called before the first frame update
    public bool beingParried,sloDaun;
    public float ParryTime = 2;
    public ParticleSystem onCollisionParticle;

    Rigidbody2D rb;
    void Start()
    {
        beingParried = false;
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (!beingParried)
        {
            rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -5f, 5f), Mathf.Clamp(rb.velocity.y, -5f, 5f));
        } else
        {
            return;
        }
    }

    public void SlowDownBall()
    {
        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -5f, 5f), Mathf.Clamp(rb.velocity.y, -5f, 5f));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bounds" || collision.gameObject.tag == "Player")
        {
            onCollisionParticle.Play();
            FMODUnity.RuntimeManager.PlayOneShot("event:/WallHit");
        }

    }

    public void CallTargets1()
    {
        GameObject[] t1Targets = GameObject.FindGameObjectsWithTag("Team1");
        for (int i = 0; i < t1Targets.Length; i++)
        {
            t1Targets[i].GetComponent<Target>().SubtractTargetCount();
            t1Targets[i].GetComponent<Target>().ShrinkTarget();
        }
    }

    public void CallTargets2()
    {
        GameObject[] t2Targets = GameObject.FindGameObjectsWithTag("Team2");
        for (int i = 0; i < t2Targets.Length; i++)
        {
            t2Targets[i].GetComponent<Target>().SubtractTargetCount();
            t2Targets[i].GetComponent<Target>().ShrinkTarget();
        }
    }
}
