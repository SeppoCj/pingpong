﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GamepadInput;

public class GameManager : MonoBehaviour
{
    private int randomSelect;
    private int collisionCount = 0;

    private GameObject[] colorObjects;
    public GameObject[] players;

    [SerializeField] private PlayerController p1Script;
    [SerializeField] private PlayerController p2Script;
    [SerializeField] private PlayerController p3Script;
    [SerializeField] private PlayerController p4Script;
    [SerializeField] private GameObject backGround;
    [SerializeField] private float timer = 0.1f;
    [SerializeField] private float currentTime;
    [SerializeField] private bool timeHeld;
    public bool isBlue;
    public bool teamOneWins = false;
    public bool teamTwoWins = false;
    private bool complete = false;
    [SerializeField] private bool paused = false;
    [SerializeField] private Text teamOneText;
    [SerializeField] private Text teamTwoText;
    [SerializeField] private Text pauseText;




    private void Awake()
    {
        paused = false;
        ChangePlayers();
        
        randomSelect = Random.Range(0, 2);
        iTween.MoveTo(backGround, iTween.Hash("position", Vector3.zero, "time", 1.5f, "ignoretimescale", true, "oncompletetarget", this.gameObject, "oncomplete", "CompletePalette"));
        StartCoroutine(SwitchPalette());
        colorObjects = GameObject.FindGameObjectsWithTag("Colorable");
        players = GameObject.FindGameObjectsWithTag("Player");

        teamOneText.enabled = false;
        teamTwoText.enabled = false;
        pauseText.enabled = false;

        if (randomSelect > 0)
        {
           
            backGround.transform.position = new Vector3(-20, 0, 0);
            if (!p1Script.isAttacker && !p4Script.isAttacker)
            {
                p1Script.isAttacker = true;
                p4Script.isAttacker = true;
            }

            if (p2Script.isAttacker && p3Script.isAttacker)
            {
                p2Script.isAttacker = false;
                p3Script.isAttacker = false;
            }

            for (int i = 0; i < colorObjects.Length; i++)
            {
                colorObjects[i].GetComponent<ColorController>().isBlue = true;
                isBlue = true;
            }
        }

        if (randomSelect == 0)
        {
            backGround.transform.position = new Vector3(20, 0, 0);
            if (p1Script.isAttacker && p4Script.isAttacker)
            {
                p1Script.isAttacker = false;
                p4Script.isAttacker = false;
            }

            if (!p2Script.isAttacker && !p3Script.isAttacker)
            {
                p2Script.isAttacker = true;
                p3Script.isAttacker = true;
            }

            for (int i = 0; i < colorObjects.Length; i++)
            {
                colorObjects[i].GetComponent<ColorController>().isBlue = false;
                isBlue = false;
            }
            for (int i = 0; i < players.Length; i++)
            {
                players[i].SetActive(true);
            }
        }
        timeHeld = false;
    }

    void Update()
    {
        PauseGame();
        QuitGame();
        PlayerOneRestart();
        Debug.Log(randomSelect);
        if (Time.time > currentTime + timer && timeHeld && (!teamOneWins || !teamTwoWins))
        {
            StartCoroutine(SwitchPlayers());
            
            collisionCount = 0;
            timeHeld = false;
        }

        if (Time.time > currentTime + timer && teamOneWins)
        {
            timeHeld = false;
            teamTwoText.enabled = true; ;
            //Time.timeScale = 0;
        }

        if (Time.time > currentTime + timer && teamTwoWins)
        {
            timeHeld = false;
            teamOneText.enabled = true; ;
            //Time.timeScale = 0;
        }

        if (teamOneWins || teamTwoWins)
        {
            for (int i = 0; i < players.Length; i++)
            {
                Destroy(players[i].gameObject);
            }
            RestartGame();
        }
    }

    public void CountCollision()
    {
        currentTime = Time.time;
        if (collisionCount < 4)
        {
            collisionCount += 1;
        }

        if (collisionCount >= 4)
        {
            timeHeld = true;
        }
    }
    void ChangeValues()
    {
        Debug.Log("Shrinking?");
        iTween.ScaleTo(backGround, iTween.Hash("scale", Vector3.zero, "speed", 1f, "ignoretimescale", true, "oncompletetarget", this.gameObject, "oncomplete", "EnlargeBackground"));




    }
    void ChangeColors()
    {

        if (!teamOneWins && !teamTwoWins)
        {
            isBlue = !isBlue;
            for (int i = 0; i < colorObjects.Length; i++)
            {

                colorObjects[i].GetComponent<ColorController>().SwitchColor();
            }
        }
    }

    void ChangePlayers()
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].GetComponent<PlayerController>().isAttacker)
            {
                Debug.Log(players[i].GetComponent<PlayerController>().playerSprites[0]);
                players[i].GetComponent<PlayerController>().isAttacker = false;
                players[i].transform.localScale = players[i].GetComponent<PlayerController>().pingerScale;
                players[i].GetComponent<PlayerController>().spriteRenderer.sprite = players[i].GetComponent<PlayerController>().playerSprites[1];
            }
            else if (!players[i].GetComponent<PlayerController>().isAttacker)
            {
                players[i].GetComponent<PlayerController>().isAttacker = true;
                players[i].transform.localScale = players[i].GetComponent<PlayerController>().playerScale;
                players[i].GetComponent<PlayerController>().spriteRenderer.sprite = players[i].GetComponent<PlayerController>().playerSprites[0];
            }
        }
    }




    public void TeamOneWin()
    {
        if (!timeHeld)
        {
            currentTime = Time.time;
            timeHeld = true;
        }
        GameObject ball = GameObject.Find("Ball");
        Destroy(ball.gameObject);
        teamOneWins = true;
    }

    public void TeamTwoWin()
    {
        if (!timeHeld)
        {
            currentTime = Time.time;
            timeHeld = true;
        }
        GameObject ball = GameObject.Find("Ball");
        Destroy(ball.gameObject);
        teamTwoWins = true;
    }

    void RestartGame()
    {
        if (GamePad.GetButtonDown(GamePad.Button.Start, GamePad.Index.Any))
        {
            Time.timeScale = 1;
            paused = false;
            SceneManager.LoadScene("PlayLoop");
        }
    }

    void PlayerOneRestart()
    {
        if (GamePad.GetButtonDown(GamePad.Button.Back, GamePad.Index.One))
        {
            Time.timeScale = 1;
            paused = false;
            SceneManager.LoadScene("PlayLoop");
            
        }
    }

    void PauseGame()
    {
        if (!teamOneWins && !teamTwoWins)
        {

            if (GamePad.GetButtonDown(GamePad.Button.Start, GamePad.Index.Any) && !paused && (!teamOneWins || !teamTwoWins))
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/Pause");
                paused = true;
                Time.timeScale = 0;
                pauseText.enabled = true;
            }
            else if (GamePad.GetButtonDown(GamePad.Button.Start, GamePad.Index.Any) && paused && (!teamOneWins || !teamTwoWins))
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/UnPause");
                paused = false;
                Time.timeScale = 1;
                pauseText.enabled = false;
            }
        }

    }

    void QuitGame()
    {
       if (Input.GetButtonDown("Cancel"))
        {
            Application.Quit();
        }
    }

    private void CompletePalette()
    {


        teamOneText.enabled = false;
        teamTwoText.enabled = false;
        pauseText.enabled = false;
        teamOneWins = false;
        teamTwoWins = false;
        Time.timeScale = 1;
        backGround.GetComponent<SpriteRenderer>().sortingOrder = -1;
        complete = true;
    }

    private void EnlargeBackground()
    {
        if (!teamOneWins && !teamTwoWins)
        {
            Debug.Log("Enlarge?!");
            ChangeColors();
            ChangePlayers();
            FMODUnity.RuntimeManager.PlayOneShot("event:/ColourChange");
            iTween.ScaleTo(backGround, iTween.Hash("scale", new Vector3(0.4f, 0.2f, 1f), "speed", 1f, "ignoretimescale", true, "oncompletetarget", this.gameObject, "oncomplete", "CompleteSwitch"));
        }
    }


    private void CompleteSwitch()
    {
        Debug.Log("Complete?!");
        
        Time.timeScale = 1;
        complete = true;

    }
    private IEnumerator SwitchPalette()
    {

        yield return new WaitUntil(() => complete == true);
        complete = false;
    }
    private IEnumerator SwitchPlayers()
    {
        Time.timeScale = 0.05f;
        ChangeValues();
        yield return new WaitUntil(() => complete == true);
        complete = false;
    }

}