﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEffects : MonoBehaviour
{
    Vector3 shakeVector = new Vector3(1, 1, 0);
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShakeCamera()
    {
        iTween.ShakePosition(this.gameObject, iTween.Hash("amount", shakeVector, "time", 1f, "ignoretimescale", true));
    }
}
