﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingScript : MonoBehaviour
{
    [SerializeField]
    float shrinkSpeed = 0.005f;
    [SerializeField]
    float initialSize = 0.15f;
    public GameObject background;

    // Start is called before the first frame update
    void Start()
    {
        background = GameObject.Find("Background");
        transform.localScale = new Vector3(initialSize, initialSize, 0);
        if (background.GetComponent<ColorController>().isBlue)
        {
            this.GetComponent<ColorController>().isBlue = false;
        }
        else {
            this.GetComponent<ColorController>().isBlue = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = transform.localScale - new Vector3(shrinkSpeed, shrinkSpeed, 0);

        if (transform.localScale.x <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
