﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamepadInput;

public class Parry : MonoBehaviour
{

   
    public bool parryable, wasParried = false;

    [SerializeField] private GameObject ball;

    [SerializeField] private PlayerController playerController;

    [SerializeField] private readonly float multiplier = 1.03f;
    [SerializeField] private float parryTime = 0.5f;
    [SerializeField] private float resetParryTime = 2f;
    [SerializeField] private float setTime;

    

    private void Start()
    {
        parryable = true;
    }

    void Update()
    {

        if (parryable)
        {
            if (playerController.isAttacker)
            {
                if (GamePad.GetButtonDown(GamePad.Button.A, playerController.playerIndex))
                {
                    setTime = Time.time;
                    wasParried = true;
                    parryable = false;
                }
            }
        }

        if (Time.time > setTime + parryTime)
        {
            
            wasParried = false;
            
        }

        if (Time.time > setTime + resetParryTime)
        {
            parryable = true;
        }
    }

    public void SpeedUpBall()
    {
        ball.GetComponent<Rigidbody2D>().velocity *= multiplier;

        wasParried = false;
    }
}
