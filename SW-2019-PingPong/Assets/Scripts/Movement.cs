﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float rotateSpeed = 5;
    //float currentSpeed;
    [SerializeField]
    float speed = 1;
    float pingerSpeed = 10f;
    //Vector3 currentVelocity;
    Rigidbody2D rb;
    //PlayerController p1ControllerRef;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //p1ControllerRef = GetComponent<PlayerController>();
    }

    public void MovePlayer(float h, float v)
    {
        rb.velocity = new Vector2(h, v) * speed;
    }


    public void MovePinger(float h, float v)
    {
        rb.velocity = new Vector2(h, v) * pingerSpeed;
    }

    public void Rotate(string triggerPressed)
    {

        if (triggerPressed == "Right")
        {
            transform.rotation = transform.rotation * Quaternion.Euler(0, 0, -rotateSpeed);
        }


        if (triggerPressed == "Left")
        {
            
            transform.rotation = transform.rotation * Quaternion.Euler(0, 0, rotateSpeed);

        }

    }

}


