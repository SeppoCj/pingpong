﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public GameManager gmScript;
    public int targetID;
    public ParticleSystem explosion;
    GameObject[] otherTargets;
    GameObject ball;
    Ball ballScript;
    int targetCount;
    // Start is called before the first frame update
    void Start()
    {
        ball = GameObject.Find("Ball");
        ballScript = ball.GetComponent<Ball>();
        if (this.targetID == 1)
        {
            otherTargets = GameObject.FindGameObjectsWithTag("Team1");
        }
        else if (this.targetID == 2)
        {
            otherTargets = GameObject.FindGameObjectsWithTag("Team2");
        }
        targetCount = otherTargets.Length;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(targetCount);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {

            
            Instantiate(explosion, transform.position, Quaternion.identity);
            FMODUnity.RuntimeManager.PlayOneShot("event:/TargetHit");
            GameObject cam = GameObject.FindGameObjectWithTag("MainCamera");
            cam.GetComponent<CameraEffects>().ShakeCamera();

            if (this.targetID == 1 && targetCount < 2)
            {
                gmScript.TeamOneWin();
                Destroy(this.gameObject);
            }
            else if (this.targetID == 1 && targetCount >= 2)
            {
                ballScript.CallTargets1();
                Destroy(this.gameObject);
            }

            if (this.targetID == 2 && targetCount < 2)
            {
                gmScript.TeamTwoWin();
                Destroy(this.gameObject);
            }
            else if (this.targetID == 2 && targetCount >= 2)
            {
                ballScript.CallTargets2();
                Destroy(this.gameObject);
            }

            
        }
    }

    public void SubtractTargetCount()
    {
        targetCount -= 1;
    }

    public void ShrinkTarget()
    {
        Vector3 scale = this.transform.localScale;
        this.transform.localScale = scale / 1.3f;
        
    }
}