﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamepadInput;

public class PlayerController : MonoBehaviour
{
    public Sprite[] playerSprites;
    [HideInInspector] public SpriteRenderer spriteRenderer;
    [SerializeField] private Color tintBlue, tintRed;
    public bool isAttacker;

    public GamePad.Index playerIndex;
    public Vector3 playerScale = new Vector3(0.08f, 0.08f, 0);
    public Vector3 pingerScale = new Vector3(0.25f, 0.25f, 0);

    [SerializeField] private GameObject ball;
    [SerializeField] private GameObject pingObject;
    [SerializeField] private GameManager gameManager;

    [SerializeField] private GameObject parry;
    public bool ballHit;
    public bool isBlue;
    
    private Movement moveScript;
    private float horizontal, vertical;
    private bool rotating = false;

    private BoxCollider2D boxCollider;
    public string lastTriggerPressed;

    float speedUpTime = 0.1f;
    float heldTime;
    bool speedUpTimerActive;
    
    [SerializeField] Transform end1;
    [SerializeField] Transform end2;
    

    void Start()
    {
        moveScript = GetComponent<Movement>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();
        speedUpTimerActive = false;
        

        if (isAttacker){
            transform.localScale = playerScale;
        }
        else{
            transform.localScale = pingerScale;
        }
    }

    void Update()
    {
        
        isBlue = gameManager.isBlue;
        horizontal = GamePad.GetAxis(GamePad.Axis.LeftStick, playerIndex).x;
        vertical = GamePad.GetAxis(GamePad.Axis.LeftStick, playerIndex).y;

        if(isAttacker)
        { 
            parry.GetComponent<BoxCollider2D>().enabled = true;
            boxCollider.enabled = true;

            spriteRenderer.sprite = playerSprites[0];
            spriteRenderer.color = Color.black;
            SpeedUp();

            if (GamePad.GetTrigger(GamePad.Trigger.RightTrigger, playerIndex) >= 0.8){
                rotating = true;
                lastTriggerPressed = "Right";
            }
            if (GamePad.GetTrigger(GamePad.Trigger.LeftTrigger, playerIndex) >= 0.8){
                rotating = true;
                lastTriggerPressed = "Left";
            }
            if (GamePad.GetTrigger(GamePad.Trigger.RightTrigger, playerIndex) >= 0.8 && (GamePad.GetTrigger(GamePad.Trigger.LeftTrigger, playerIndex) >= 0.8)){
                rotating = false;
            }
            if (GamePad.GetTrigger(GamePad.Trigger.RightTrigger, playerIndex) <= 0.4 && (GamePad.GetTrigger(GamePad.Trigger.LeftTrigger, playerIndex) <= 0.4)){
                rotating = false;
            }
            
        }

        else
        {
            spriteRenderer.sprite = playerSprites[1];
            transform.rotation = Quaternion.identity;
            parry.GetComponent<BoxCollider2D>().enabled = false;
            boxCollider.enabled = false;

            if (isBlue){
                spriteRenderer.color = tintBlue;
            }
            else{
                spriteRenderer.color = tintRed;
            }

            if (GamePad.GetButtonDown(GamePad.Button.A, playerIndex)){
                FMODUnity.RuntimeManager.PlayOneShot("event:/Ping");
                Instantiate(pingObject, transform.position, Quaternion.identity);
            }
        }
    }

    private void FixedUpdate()
    {
        if (playerIndex == GamePad.Index.One || playerIndex == GamePad.Index.Two){
            this.transform.position = new Vector2(Mathf.Clamp(this.transform.position.x, -9.5f, 0f), Mathf.Clamp(this.transform.position.y, -5f, 5f));
        }

        if (playerIndex == GamePad.Index.Three || playerIndex == GamePad.Index.Four){
            this.transform.position = new Vector2(Mathf.Clamp(this.transform.position.x, 0, 9.5f), Mathf.Clamp(this.transform.position.y, -5f, 5f));
        }

        if (rotating == true){
            moveScript.Rotate(lastTriggerPressed);
        }

        if (isAttacker){
            moveScript.MovePlayer(horizontal, vertical);   
        }

        if (!isAttacker){   
            moveScript.MovePinger(horizontal, vertical);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            Parry parryScript = parry.GetComponent<Parry>();
            Ball ballScript = ball.GetComponent<Ball>();

            
            
            if (parryScript.wasParried)
            {
                boxCollider.enabled = false;
                heldTime = Time.time;
                speedUpTimerActive = true;
                ballScript.beingParried = true;
                Shake();
               FMODUnity.RuntimeManager.PlayOneShot("event:/ParryHit");
            } else if (!parryScript.wasParried)
            {
                ballScript.beingParried = false;
                ballScript.SlowDownBall();
                FMODUnity.RuntimeManager.PlayOneShot("event:/RegularHit");
            }
        }
    }

   void SpeedUp()
    {
        if (Time.time > heldTime + speedUpTime && speedUpTimerActive)
        {
            Parry parryScript = parry.GetComponent<Parry>();
            parryScript.SpeedUpBall();
            speedUpTimerActive = false;
        }
    }

    public void Shake()
    {
        iTween.ShakePosition(this.gameObject, iTween.Hash("amount", new Vector3(0.5f, 0.5f, 0.5f), "time", 0.5f, "oncompletetarget", this.gameObject, "oncomplete", "CheckComplete"));
    }

    void Stop()
    {
        iTween.Stop("iTween.ShakePosition");
    }

    private void CheckComplete()
    {
        boxCollider.enabled = true;
    }
}
